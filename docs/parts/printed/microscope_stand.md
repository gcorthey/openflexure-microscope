# Microscope Stand
The stand will hold the microscope flat on a table, and can also house a Raspberry Pi computer to control the microscope.  It can optionally stack on top of the [motor driver case](./motor_driver_case.md) if you need to mount a motor controller.

## STL Files
There are two versions:
* ``microscope_stand.stl`` holds a Raspberry Pi.
* ``microscope_stand_no_pi`` is less tall, and doesn't hold the Pi.
