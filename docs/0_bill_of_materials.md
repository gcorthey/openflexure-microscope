# Obtaining the non-printed parts
Make sure you have all the necessary parts and tools before you start.  The parts should all be listed in the bill of materials below, which is currently a work in progress (eventually it will be produced automatically, which should help it to stay accurate).  We recommend reading through all the instructions and checking that the quantities shown here add up correctly, until auto-generation is working.  Printed parts are described in the [next section](./0_printing.md).

## Partes para fijación:
*   3x [Tornillos allen de acero inoxidable de M3x25 mm]()
*   4x [Tuerca M3 de latón o acero]()
*   8x [Arandela M3 de acero]()
*   5x [Tornillos allen M3x8 mm]() 
*   4x [Tornillos allen M2x6 mm]() 
*   3x [O-rings de 30x2 mm]()

## Partes electrónicas:
*   [LED blanco de alta luminosidad de 5mm](https://articulo.mercadolibre.com.ar/MLA-619433772-pack-50-led-5mm-blanco-alta-luminosidad-_JM?quantity=1)
*   [Raspbery Pi Zero con módulo de cámara](https://articulo.mercadolibre.com.ar/MLA-756108400-raspberry-pi-zero-w-kit-e14-rs-rpi0w-base-_JM) (v2, though v1 works too)
*   [Memoria micro sd de 16 gb](https://articulo.mercadolibre.com.ar/MLA-631744587-memoria-micro-sd-hc-16-gb-kingston-clase-10-tienda-_JM?quantity=1)
*   [Cable HDMI]()
*   [Monitor LCD o LED con entrada HDMI]()


## Herramientas:
*   Llaves allen de 1.5 y 2.5 mm 
*   Cinta aisladora
*   Cutter
*   Alicate


Don't forget the raspberry pi, camera module, and associated screen, power supply, SD card, keyboard, mouse, etc. (I have not listed these explicitly, but they're needed to run the Pi). Also, if you use the high resolution optics module or want to add motors, you will need additional parts.

While you're waiting for any parts you have ordered to arrive, why not try the next section on [printing the plastic parts](./0_printing.md).